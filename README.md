Coding standards
=================
### Installation

## 1. Link the CS repository
   
    "repositories": [        
		{
			"type": "vcs",
			"url": "https://bitbucket.org/langur8/codingstandard"
        }
    ]

## 2. Add the repository to the `dev` dependencies

    "require-dev": {
        "dlouhy/codingstandard": "dev-master"
	}

## 3. Install the repository

    composer update dlouhy/coding-standard

## 4. Copy the `pre-commit` hook
Run the following command in theroot folder (where .git folder is located) 

**Warning: this commnad will override your git pre-commit file**

    bash ${composer_folder}/vendor/dlouhy/coding-standard/src/bin/setup.sh ${composer_folder}
       
### Examples
if composer.json is in root folder (same as .git folder)
        
    bash ./vendor/dlouhy/coding-standard/src/bin/setup.sh .
    
if composer.json is in application folder
        
    bash application/vendor/dlouhy/coding-standard/src/bin/setup.sh application

    
## 5. Copy templates
Copy configuration definition and Makefile into your project. There are two templates, one with the specific 
configuration for Laravel projects, second for all others. 
    
    # Makefile
    cp ${composer_folder}/vendor/dlouhy/coding-standard/templates/Makefile ${composer_folder}/Makefile
    
    # PHPStan configuration for Laravel 
    cp ${composer_folder}/vendor/dlouhy/coding-standard/templates/phpstan-laravel.neon ${composer_folder}/phpstan.neon
    
    # Common PHPStan configuration 
    cp ${composer_folder}/vendor/dlouhy/coding-standard/templates/phpstan-common.neon ${composer_folder}/phpstan.neon
    
## 6. Makefile Customization
You can customize your Makefile for your project as you want. 

There are example configuration variables at the beginning of the Makefile.

The second part is a little hack of base makefile specification, which converts second and
others targets into variable `DIR_PARAM`, that can be used for the easier definition of target paths (i.e. `make codestyle foo boo` 
will run code style check on foo and boo directories instead of directories defined in DIR_CS variable). 

The third part of the Makefile is a list of make targets, which are shortcuts of some complex commands. The main purpose of the Makefile is to
simplify running frequently triggered tasks with many parameters - be free to edit this file as you wish.  

## PhpStorm integration
At first we need to setup Configuration for sniffer this way: 

![sniffer](images/sniffer.png)

Second enable Editor inspections, here we need to choose custom standard, select ruleset xml file (`vendor/dlouhy/coding-standard/src/cs.xml`) and then verify by the **refresh icon**.
 
![inspection](images/inspection.png)

Setup mess detector is similar, only in the Mess detector categories (`vendor/dlouhy/coding-standard/src/md.xml`). 


Useful commands
------------------

To manually check standards

    make codestyle ${folder_or_file}

To automatically fix some bugs

    make fix-codestyle ${folder_or_file}

To automatically detect code mess

    make messdetector ${folder_or_file}

To automatically do static analysis

    make ${LEVEL=1} phpstan ${folder_or_file}
    
### Laravel specific configuration

In Laravel applications you have to include package:

    composer require --dev weebly/phpstan-laravel

## Sniffs
## Rules checked with [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer)

| Rule name                                                                         | Rule description                                                                                                                                                                                         
| ---------------------------------------------                                     | ----------------------------------------------------------------------------------------------------------------------------------                                                                       
| `Generic.CodeAnalysis.EmptyStatement`                                             | Bans the use of the PHP long array syntax.                                                                                                                                                               
| `Generic.Arrays.DisallowLongArraySyntax`                                          | You should not use empty statements, only empty `catch` is allowed.                                                                                                                                   
| `Generic.CodeAnalysis.ForLoopShouldBeWhileLoop`                                   | Detects for-loops that can be simplified to a while-loop.                                                                                                                                                
| `Generic.CodeAnalysis.ForLoopWithTestFunctionCall`                                | Detects for-loops that use a function call in the test expression.                                                                                                                                       
| `Generic.CodeAnalysis.JumbledIncrementer`                                         | Detects incrementer jumbling in for loops. Ie. the usage of one and the same incrementer into an inner and outer loop. Even it is intended this is confusing code.                                       
| `Generic.CodeAnalysis.UnconditionalIfStatement`                                   | You should avoid `if` statements that are always `true` or `false`.                                                                                                                                      
| `Generic.CodeAnalysis.UselessOverridingMethod`                                    | Detects unnecessary overridden methods that simply call their parent.                                                                                                                                    
| `Generic.Commenting.Todo`                                                         | Warns about `TODO` comments.                                                                                                                                                                             
| `Generic.Commenting.Fixme`                                                        | Warns about `FIXME` comments.                                                                                                                                                                            
| `Generic.Formatting.NoSpaceAfterCast`                                             | Ensures there is no space after cast tokens.                                                                                                                                                             
| `Generic.Formatting.DisallowMultipleStatements`                                   | Ensures each statement is on a line by itself.                                                                                                                                                     
| `Generic.Formatting.NoSpaceAfterCast`                                             | Ensures there is no space after cast tokens.                                                                                                                                                     
| `Generic.Metrics.CyclomaticComplexity`                                            | Checks the cyclomatic complexity (McCabe) for functions.                                                                                                                                                     
| `Generic.Metrics.NestingLevel`                                                    | Checks the nesting level for methods.                                                                                                                                                 
| `Generic.Functions.CallTimePassByReference`                                       | Ensures that variables are not passed by reference when calling a function.                                                                                                                              
| `Generic.NamingConventions.ConstructorName`                                       | Favor PHP 5 constructor syntax, which uses function `__construct()`. Avoid PHP 4 constructor syntax, which uses function `ClassName()`.                                                                  
| `Generic.NamingConventions.CamelCapsFunctionName`                                 | Method names should be in camelCase (except for PHP magic methods and PHP non-magic methods starting with a double underscore).                                                                          
| `Generic.PHP.NoSilencedErrors`                                                    | You should not use errors silencing using `@` sign.                                                                                                                                                      
| `Generic.PHP.LowerCaseConstant`                                                   | Checks that all uses of true, false and null are lowercase.                                                                                                                                                      
| `Generic.Files.EndFileNewline`                                                    | Ensures the file ends with a newline character.                                                                                                                                                          
| `Generic.Files.OneClassPerFile`                                                   | Checks that only one class is declared per file.                                                                                                                                              
| `Generic.Files.OneInterfacePerFile`                                               | Checks that only one interface is declared per file.                                                                                                                                             
| `Generic.Files.OneTraitPerFile`                                                   | Checks that only one trait is declared per file.                                                                                                                                        
| `Generic.WhiteSpace.ScopeIndent`                                                  | Checks that control structures are defined and indented correctly.                                                                                                                                        
| `MySource.PHP.GetRequestData`                                                     | The request super globals (`$_REQUEST`, `$_GET`, `$_POST`, `$_FILES`) must not be accessed directly.                                                                                                     
| `PEAR.Commenting.InlineComment`                                                   | Checks that no perl-style (starting with `#` symbol) comments are used.                                                                                                                                  
| `PEAR.NamingConventions.ValidClassName`                                           | Ensures class and interface names start with a capital letter.                                                                                                                                           
| `PEAR.WhiteSpace.ObjectOperatorIndent`                                            | Checks that object operators are indented correctly.                                                                                                                                           
| `PEAR.WhiteSpace.ScopeClosingBrace`                                               | Checks that the closing braces of scopes are aligned correctly.                                                                                                                                           
| `PEAR.Functions.FunctionDeclaration`                                              | Ensure single and multi-line function declarations are defined correctly.                                                                                                                                                                                              
| `PEAR.Functions.ValidDefaultValue`                                                | Ensures function params with default values are at the end of the declaration.                                                                                                                                                                                              
| `Squiz.PHP.DisallowSizeFunctionsInLoops`                                          | Bans the use of size-based functions (`sizeof`, `strlen`, `count`) in loop conditions.                                                                                                                   
| `Squiz.PHP.Eval`                                                                  | The use of `eval()` is discouraged.                                                                                                                                                                      
| `Squiz.PHP.GlobalKeyword`                                                         | The usage of the `global` keyword is discouraged.                                                                                                                                                        
| `Squiz.PHP.InnerFunctions`                                                        | Ensures that functions within functions are never used.                                                                                                                                                  
| `Squiz.PHP.LowercasePHPFunctions`                                                 | Ensures all calls to inbuilt PHP functions are lowercase.                                                                                                                                                
| `Squiz.PHP.NonExecutableCode`                                                     | Warns about code that can never been executed. This happens when a function returns before the code, or a break ends execution of a statement etc.                                                       
| `Squiz.Scope.StaticThisUsage`                                                     | Checks for usage of `$this` in static methods, which would cause runtime errors.                                                                                                                         
| `Squiz.Strings.DoubleQuoteUsage`                                                  | Makes sure that any use of double quotes is warranted.                                                                                                                                                   
| `Squiz.WhiteSpace.CastSpacing`                                                    | Ensures cast statements do not contain whitespace.                                                                                                                                                       
| `Squiz.WhiteSpace.LanguageConstructSpacing`                                       | Ensures all language constructs (without brackets - `echo`, `print`, `return`, `include`, `include_once`, `require`, `require_once`, `new`) contain a single space between themselves and their content. 
| `Squiz.WhiteSpace.LogicalOperatorSpacing`                                         | Verifies that operators have valid spacing (ie. one space) surrounding them.                                                                                                                               
| `Squiz.WhiteSpace.FunctionSpacing`                                                | Checks the separation between methods in a class or interface.                                                                                                                               
| `SlevomatCodingStandard.TypeHints.DeclareStrictTypes`                             | Enforces having `declare(strict_types = 1)` at the top of each PHP file. Allows configuring how many newlines should be between the `<?php` opening tag and the declare statement.
| `SlevomatCodingStandard.ControlStructures.DisallowYodaComparison`                 | Yoda conditions decrease code comprehensibility and readability by switching operands around comparison operators forcing the reader to read the code in an unnatural way.              
| `SlevomatCodingStandard.ControlStructures.DisallowEqualOperators`                 | Disallows using loose `==` and `!=` comparison operators. Use `===` and `!==` instead, they are much more secure and predictable.
| `SlevomatCodingStandard.Namespaces.UnusedUses`                                    | Looks for unused imports from other namespaces.
| `SlevomatCodingStandard.Namespaces.UseFromSameNamespace`                          | Prohibits uses from the same namespace.
| `SlevomatCodingStandard.Exceptions.DeadCatch`                                     | This sniff finds unreachable catch blocks.
| `SlevomatCodingStandard.Arrays.TrailingArrayComma`                                | Commas after last element in an array make adding a new element easier and result in a cleaner versioning diff. 
| `SlevomatCodingStandard.ControlStructures.LanguageConstructWithParentheses`       | `LanguageConstructWithParenthesesSniff` checks and fixes language construct used with parentheses.
| `SlevomatCodingStandard.TypeHints.LongTypeHints`                                  | Enforces using shorthand scalar typehint variants in phpDocs: `int` instead of `integer` and `bool` instead of `boolean`. This is for consistency with native scalar typehints which also allow shorthand variants only.
| `SlevomatCodingStandard.Classes.ClassConstantVisibility`                          | In PHP 7.1 it's possible to declare visibility of class constants. In a similar vein to optional declaration of visibility for properties and methods which is actually required in sane coding standards, this sniff also requires declaring visibility for all class constants.                                                                                                 
| `SlevomatCodingStandard.TypeHints.ReturnTypeHintSpacing`                          | Enforces consistent formatting of return typehints. 
| `SlevomatCodingStandard.TypeHints.NullableTypeForNullDefaultValue`                | Checks whether the nullablity `?` symbol is present before each nullable and optional parameter (which are marked as `= null`): 
| `SlevomatCodingStandard.TypeHints.ParameterTypeHintSpacing`                       | Checks that there's a single space between a typehint and a parameter name: `Foo $foo` + Checks that there's no whitespace between a nullability symbol and a typehint: `?Foo`
| `SlevomatCodingStandard.Namespaces.DisallowGroupUse`                              | Group use declarations are ugly, make diffs ugly and this sniff prohibits them.                                                                                                                                                                                                          

## Mess Detector
## Rules checked with [PHPMD](https://github.com/phpmd/phpmd)

| Rule name                                                                         | Rule description                                                                                                                                                                                         
| ---------------------------------------------                                     | ----------------------------------------------------------------------------------------------------------------------------------
| `rulesets/cleancode.xml/ElseExpression`                                           | An if expression with an else branch is never necessary. You can rewrite the conditions in a way that the else is not necessary and the code becomes simpler to read. To achieve this use early return statements. To achieve this you may need to split the code it several smaller methods. For very simple assignments you could also use the ternary operations.
| `rulesets/codesize.xml/CyclomaticComplexity`                                      | Complexity is determined by the number of decision points in a method plus one for the method entry. The decision points are 'if', 'while', 'for', and 'case labels'. Generally, 1-4 is low complexity, 5-7 indicates moderate complexity, 8-10 is high complexity, and 11+ is very high complexity.
| `

