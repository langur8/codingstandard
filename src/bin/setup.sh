#!/bin/sh

cat $1/vendor/databreakers/coding-standards/src/bin/pre-commit.template | sed -e "s/\${composer_folder}/$1/" > .git/hooks/pre-commit

chmod +x .git/hooks/pre-commit

