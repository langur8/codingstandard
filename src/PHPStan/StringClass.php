<?php declare(strict_types=1);

namespace dlouhy\Tools\PHPStan;

use PhpParser\Node;
use PhpParser\Node\Scalar\String_;
use PHPStan\Analyser\Scope;
use PHPStan\Broker\Broker;
use PHPStan\Rules\Rule;

final class StringClass implements Rule
{
    /**
     * @return string
     */
    public function getNodeType(): string
    {
        return String_::class;
    }

    /**
     * @param Node  $node
     * @param Scope $scope
     * @return string[] errors
     * @throws \PHPStan\ShouldNotHappenException
     * @throws \PHPStan\Broker\ClassAutoloadingException
     */
    public function processNode(Node $node, Scope $scope): array
    {
        if (($node instanceof String_) === false) {
            return [];
        }

        if ($this->isPossibleClass($node->value) === false) {
            return [];
        }

        if (Broker::getInstance()->hasClass($node->value) === true) {
            return [
                sprintf(
                    'String "%s" can be replaced by %s::class',
                    $node->value,
                    $node->value
                ),
            ];
        }

        return [];
    }

    /**
     * Check only strings which starts with backslash (fully qualified name)
     * and contains only letters, numbers and backslashes.
     * @param string $string
     * @return bool
     */
    private function isPossibleClass(string $string): bool
    {
        return preg_match('/^\\\\[\w\\\\_]+/', $string) === 1;
    }
}
