<?php declare(strict_types=1);

namespace dlouhy\Tools\CodingStandards\Sniffs\Classes;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;

final class ClassFinalAbstractSniff implements Sniff
{
    public const MISSING_CLASS_MODIFIER = 'MissingClassModifier';

    /**
     * @return int[]
     */
    public function register(): array
    {
        return [
            T_CLASS,
        ];
    }

    /**
     * @param File $phpcsFile
     * @param int  $constantPointer
     * @throws \PHP_CodeSniffer\Exceptions\TokenizerException
     */
    public function process(File $phpcsFile, $constantPointer): void
    {
        if ($this->hasClassType($phpcsFile->getClassProperties($constantPointer)) === false) {
            $phpcsFile->addError(
                'Class has to be final or abstract',
                $constantPointer,
                self::MISSING_CLASS_MODIFIER
            );
        }
    }

    /**
     * @param array $properties
     * @return bool
     */
    private function hasClassType(array $properties): bool
    {
        return $this->isAbstract($properties) === true
            || $this->isFinal($properties) === true;
    }

    /**
     * @param array $properties
     * @return bool
     */
    private function isAbstract(array $properties): bool
    {
        return (bool) $properties['is_abstract'];
    }

    /**
     * @param array $properties
     * @return bool
     */
    private function isFinal(array $properties): bool
    {
        return (bool) $properties['is_final'];
    }
}
